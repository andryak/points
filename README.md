# Points

---

![](https://app.codeship.com/projects/00a6ded0-c171-0135-3fc4-1eef93472b99/status?branch=master "CI status")


## Abstract

Points is a RESTful API to manage a set of points on a plane
and to determine maximal line segments passing through at least
*n* of such points.

The application supports:

+ adding a point to the space,
+ removing all points from the space,
+ retrieving all points in the space,
+ calculating all maximal line segments passing through at least *n* points.

## Installation

The application is shipped as a Python 3.6 package.
It depends on the `virtualenv` and `python3.6` binaries which you might need
to install, if needed. To install the application and its dependencies clone
the repository, navigate to its main folder and run the following commands:

```Bash
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
pip install .
```

Finally, to run the application:

```Bash
python points/api.py
```

The api script accepts different options,
run `python points/api.py --help` for a complete list.

## Exposed API

When the api script is running it handles the following requests:

1. POST /point with body { "x": **int**, "y": **int** }
2. GET /space
3. GET /lines/n where n: **int**
4. DELETE /space

These commands, respectively:

1. add a new point to the space,
2. return the list of all points in the space,
3. return all maximal line segments passing through at least n points,
4. remove all points from the space.

All requests produce JSON response with appropriate status codes.
In particular:

1. returns the point added with a 201 (CREATED) status code or an
object with a field "message" describing why the point could not be
created. In the latter case the returned status code is 409 (CONFLICT),
2. returns a list of objects that can be interpreted as points. This
operation always succeeds and returns a 200 (OK) status code. The
objects in the returned list have two fields: namely, "x" and "y".
3. returns a list of objects with a single field "line_segment" containing
a list of points comprising the line segments. If the parameter n is not
a positive integer the api returns 404 (NOT_FOUND).
4. returns null. This operation always succeeds and returns a 200 (OK)
status code.

## Tests and Code Style

The application is shipped with a Makefile to run tests and check PEP8 compliancy.
Please refer to the `test` and `checkstyle` targets in the Makefile.

```Bash
make test
make checkstyle
```

## Logo

The application logo has been designed by [Smashicons](https://www.flaticon.com/authors/smashicons)
at [flaticon](http://www.flaticon.com).
