import json
import unittest
import warnings

from points.api import app
from points.api import space
from flask_api import status


class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = True
        self.app = app.test_client()

    def tearDown(self):
        # Clear the in-memory database after each test.
        space.clear()

    def add_point(self, point):
        # Disable deprecation warnings due to bug in Flask `post` method.
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore",category=DeprecationWarning)
            return self.app.post('/point', data=point)

    def test_space_is_initially_empty(self):
        response = self.app.get('/space')
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response, [])

    def test_add_malformed_point_with_missing_x_to_space(self):
        response = self.add_point({'y': 0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_malformed_point_with_missing_y_to_space(self):
        response = self.add_point({'x': 0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_malformed_point_with_wrong_type_for_y_to_space(self):
        response = self.add_point({'x': 0, 'y': 'not-a-number'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_malformed_point_with_wrong_type_for_x_to_space(self):
        response = self.add_point({'x': 'not-a-number', 'y': 0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_point_to_space_once(self):
        response = self.add_point({'x': 0, 'y': 0})
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response, {'x': 0, 'y': 0})

    def test_add_point_to_space_twice(self):
        response = self.add_point({'x': 0, 'y': 0})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.add_point({'x': 0, 'y': 0})
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)

    def test_add_different_points_to_space(self):
        response = self.add_point({'x': 0, 'y': 0})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.add_point({'x': 1, 'y': 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_clear_space(self):
        response = self.app.delete('/space')
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json_response, None)

    def test_list_empty_space(self):
        response = self.app.get('/space')
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json_response, [])

    def test_list_non_empty_space(self):
        self.add_point({'x': 1, 'y': 1})
        self.add_point({'x': 2, 'y': 2})
        self.add_point({'x': 3, 'y': 3})

        response = self.app.get('/space')
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(json_response), 3)

    def test_list_space_with_floating_point_coordinates(self):
        self.add_point({'x': 0.5, 'y': 0.5})
        self.add_point({'x': 0.25, 'y': 0.25})
        self.add_point({'x': 0.125, 'y': 0.125})

        response = self.app.get('/space')
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(json_response), 3)

        points = [(coords['x'], coords['y']) for coords in json_response]
        self.assertIn((0.5, 0.5), points)
        self.assertIn((0.25, 0.25), points)
        self.assertIn((0.125, 0.125), points)


if __name__ == "__main__":
    unittest.main()
