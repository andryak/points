import time
import unittest

from points.geometry import slope
from points.geometry import Point
from points.geometry import Space
from points.geometry import LineSegment


class TestPoint(unittest.TestCase):
    def test_init(self):
        point = Point(0, 1)
        self.assertEqual(point.x, 0)
        self.assertEqual(point.y, 1)

    def test_points_are_equal(self):
        point1 = Point(0, 1)
        point2 = Point(0, 1)
        self.assertIsNot(point1, point2)
        self.assertEqual(point1, point2)

    def test_points_are_not_equal(self):
        point1 = Point(0, 0)
        point2 = Point(0, 1)
        self.assertIsNot(point1, point2)
        self.assertNotEqual(point1, point2)

    def test_points_have_same_hash(self):
        point1 = Point(0, 0)
        point2 = Point(0, 0)
        self.assertEqual(hash(point1), hash(point2))

    def test_point_to_string(self):
        point = Point(0, 1)
        self.assertEqual(str(point), 'Point(x=0, y=1)')

    def test_point_repr(self):
        point = Point(0, 1)
        self.assertEqual(repr(point), 'Point(x=0, y=1)')


class TestPointUtils(unittest.TestCase):
    def test_slope_of_diagonal_rect(self):
        self.assertAlmostEqual(slope(Point(0, 0), Point(1, 1)), 1.0, 10)

    def test_slope_of_inverse_diagonal_rect(self):
        self.assertAlmostEqual(slope(Point(1, 1), Point(0, 0)), 1.0, 10)

    def test_slope_of_rect_parallel_to_x_axis(self):
        self.assertAlmostEqual(slope(Point(0, 0), Point(1, 0)), 0.0, 10)

    def test_slope_of_rect_parallel_to_y_axis(self):
        self.assertEqual(slope(Point(0, 0), Point(0, 1)), float('inf'))


class TestLineSegment(unittest.TestCase):
    def test_init_with_no_points(self):
        with self.assertRaises(AssertionError):
            LineSegment()

    def test_init_with_two_points(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        self.assertEqual(len(line_segment), 2)
        self.assertTrue(Point(0, 0) in line_segment)
        self.assertTrue(Point(1, 1) in line_segment)

    def test_init_with_many_points(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1), Point(2, 2))
        self.assertEqual(len(line_segment), 3)
        self.assertTrue(Point(0, 0) in line_segment)
        self.assertTrue(Point(1, 1) in line_segment)
        self.assertTrue(Point(2, 2) in line_segment)

    def test_is_collinear_with(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        self.assertTrue(line_segment.is_collinear_with(Point(2, 2)))

    def test_is_not_collinear_with(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        self.assertFalse(line_segment.is_collinear_with(Point(2, 3)))

    def test_add_collinear_point(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        line_segment.add_point(Point(2, 2))

        self.assertEqual(len(line_segment), 3)
        self.assertTrue(Point(0, 0) in line_segment)
        self.assertTrue(Point(1, 1) in line_segment)
        self.assertTrue(Point(2, 2) in line_segment)

    def test_add_non_collinear_point(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        with self.assertRaises(ValueError):
            line_segment.add_point(Point(2, 3))

    def test_len(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 0))
        self.assertEqual(len(line_segment), 2)

    def test_line_segment_contains_point(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 0))
        self.assertTrue(Point(0, 0) in line_segment)

    def test_line_segment_doest_not_contain_point(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 0))
        self.assertFalse(Point(0, 1) in line_segment)

    def test_line_segments_are_equal(self):
        line_segment1 = LineSegment(Point(0, 0), Point(1, 1))
        line_segment2 = LineSegment(Point(0, 0), Point(1, 1))
        self.assertIsNot(line_segment1, line_segment2)
        self.assertEqual(line_segment1, line_segment2)

    def test_line_segments_are_not_equal(self):
        line_segment1 = LineSegment(Point(0, 0), Point(1, 1))
        line_segment2 = LineSegment(Point(0, 0), Point(2, 2))
        self.assertIsNot(line_segment1, line_segment2)
        self.assertNotEqual(line_segment1, line_segment2)

    def test_line_segments_have_same_hash(self):
        line_segment1 = LineSegment(Point(0, 0), Point(1, 1))
        line_segment2 = LineSegment(Point(0, 0), Point(1, 1))
        self.assertEqual(hash(line_segment1), hash(line_segment2))

    def test_line_segment_to_string(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        self.assertEqual(str(line_segment), 'LineSegment(points={Point(x=0, y=0), Point(x=1, y=1)})')

    def test_line_segment_repr(self):
        line_segment = LineSegment(Point(0, 0), Point(1, 1))
        self.assertEqual(str(line_segment), 'LineSegment(points={Point(x=0, y=0), Point(x=1, y=1)})')


class TestSpace(unittest.TestCase):
    def test_empty_init(self):
        space = Space()
        self.assertIsInstance(space.points, set)
        self.assertEqual(len(space.points), 0)

    def test_init_with_points(self):
        space = Space(Point(0, 0), Point(1, 1), Point(2, 2))
        self.assertEqual(len(space.points), 3)
        self.assertTrue(Point(0, 0) in space.points)
        self.assertTrue(Point(1, 1) in space.points)
        self.assertTrue(Point(2, 2) in space.points)

    def test_add_point(self):
        space = Space()
        space.add_point(Point(0, 0))
        self.assertEqual(len(space.points), 1)
        self.assertTrue(Point(0, 0) in space.points)

    def test_clear(self):
        space = Space(Point(0, 0), Point(1, 1), Point(2, 2))
        space.clear()
        self.assertEqual(len(space.points), 0)

    def test_space_has_point(self):
        space = Space(Point(0, 0), Point(1, 1), Point(2, 2))
        self.assertTrue(Point(1, 1) in space)

    def test_space_does_not_have_point(self):
        space = Space(Point(0, 0), Point(1, 1), Point(2, 2))
        self.assertFalse(Point(-1, -1) in space)

    def test_line_segments_longer_than_zero_on_line(self):
        space = Space(
            Point(0, 0),
            Point(1, 1),
            Point(2, 2),
            Point(3, 3),
        )
        self.assertEqual(len(space.line_segments_longer_than(0)), 1)

    def test_line_segments_longer_than_zero_on_small_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(1, 0),
            Point(1, 1),
        )
        self.assertEqual(len(space.line_segments_longer_than(0)), 6)

    def test_line_segments_longer_than_one_on_small_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(1, 0),
            Point(1, 1),
        )
        self.assertEqual(len(space.line_segments_longer_than(1)), 6)

    def test_line_segments_longer_than_two_on_small_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(1, 0),
            Point(1, 1),
        )
        self.assertEqual(len(space.line_segments_longer_than(2)), 6)

    def test_line_segments_longer_than_three_on_small_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(1, 0),
            Point(1, 1),
        )
        self.assertEqual(len(space.line_segments_longer_than(3)), 0)

    def test_line_segments_longer_than_two_on_large_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(0, 2),
            Point(1, 0),
            Point(1, 2),
            Point(2, 0),
            Point(2, 1),
            Point(2, 2),
        )
        self.assertEqual(len(space.line_segments_longer_than(2)), 20)

    def test_line_segments_longer_than_three_on_large_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(0, 2),
            Point(1, 0),
            Point(1, 2),
            Point(2, 0),
            Point(2, 1),
            Point(2, 2),
        )
        self.assertEqual(len(space.line_segments_longer_than(3)), 4)

    def test_line_segments_longer_than_four_on_large_square(self):
        space = Space(
            Point(0, 0),
            Point(0, 1),
            Point(0, 2),
            Point(1, 0),
            Point(1, 2),
            Point(2, 0),
            Point(2, 1),
            Point(2, 2),
        )
        self.assertEqual(len(space.line_segments_longer_than(4)), 0)

    def test_line_segments_longer_than_two_on_trapezoid(self):
        space = Space(
            Point(0, 0),
            Point(1, 1),
            Point(2, 1),
            Point(2, 0),
        )
        self.assertEqual(len(space.line_segments_longer_than(2)), 6)

    def test_space_with_floating_point_coordinates(self):
        space = Space(
            Point(0, 0),
            Point(1.5, 1.5),
            Point(3, 3),
        )
        self.assertEqual(
            space.line_segments_longer_than(2),
            {LineSegment(Point(0, 0), Point(1.5, 1.5), Point(3, 3))}
        )

    def test_stress_test_maximal_line_segments_longer_than(self):
        space = Space()
        for i in range(30):
            space.add_point(Point(0, i))
            space.add_point(Point(99, i))
            space.add_point(Point(i, 0))
            space.add_point(Point(i, 99))

        tick = time.time()
        line_segments1 = space.line_segments_longer_than(2, mode='fast')
        tack = time.time()
        fast_time = tack - tick

        tick = time.time()
        line_segments2 = space.line_segments_longer_than(2, mode='slow')
        tack = time.time()
        slow_time = tack - tick

        self.assertEqual(line_segments1, line_segments2)
        self.assertLessEqual(fast_time, slow_time)



if __name__ == '__main__':
    unittest.main()
