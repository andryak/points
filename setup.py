from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='points',
    version='1.0.0',
    description='An RESTful api to manage a space of points',
    long_description=readme(),
    keywords='points space line segment',
    url='https://bitbucket.org/andryak/points',
    author='Andrea Aquino',
    author_email='andrex.aquino@gmail.com',
    license='GNU GPL v3',
    packages=['points'],
    include_package_data=True,
    zip_safe=False
)
