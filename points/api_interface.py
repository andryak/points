from flask_restful import fields


point_fields = {
    'x': fields.Float,
    'y': fields.Float,
}
"""Fields to serialize Point objects."""


line_segment_fields = {
    'line_segment': fields.List(
        fields.Nested(point_fields),
        attribute='points'
    ),
}
"""Fields to serialize LineSegment objects."""
