from points.api_interface import point_fields
from points.api_interface import line_segment_fields
from points.api_utils import enable_route_decorator
from points.geometry import Point
from points.geometry import Space

from decimal import Decimal
from flask import Flask
from flask_api import status
from flask_restful import abort
from flask_restful import reqparse
from flask_restful import marshal
from flask_restful import marshal_with
from flask_restful import Resource
from flask_restful import Api


# Initialize Flask to build a RESTful API.
# It is good practice to initialize it at the beginning of the script.
app = Flask(__name__)
api = enable_route_decorator(Api(app))


space = Space()
"""The space of points managed by the application."""


@api.route('/space')
class SpaceApi(Resource):
    @marshal_with(point_fields)
    def get(self):
        # The points in the space are returned sorted by x, then y.
        return list(sorted(space.points, key=lambda point: (point.x, point.y)))

    def delete(self):
        space.clear()
        # Implicitly return `None` which is automatically serialized to `null`.


point_parser = reqparse.RequestParser(bundle_errors=True)
point_parser.add_argument('x', type=Decimal, required=True, help="Parameter is missing or is not an integer")
point_parser.add_argument('y', type=Decimal, required=True, help="Parameter is missing or is not an integer")


@api.route('/point')
class PointApi(Resource):
    @marshal_with(point_fields)
    def post(self):
        args = point_parser.parse_args(strict=True)

        # Both parameters have been validated and transformed into integers by the parser.
        x = args['x']
        y = args['y']
        point = Point(x, y)

        if point in space:
            # If the point already exists ignore the request and return a Conflict status code.
            abort(status.HTTP_409_CONFLICT, message=f'{point} already exists.')

        # Add the point to the space and return the point with a Created status code.
        space.add_point(point)
        return point, status.HTTP_201_CREATED


@api.route('/lines/<int:n>')
class LinesApi(Resource):
    def get(self, n):
        line_segments = list(space.line_segments_longer_than(n))

        # Flatten the marshalled objects dropping the 'line_segment' wrappers.
        response = marshal(line_segments, line_segment_fields)
        response = [obj['line_segment'] for obj in response]
        return response


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='RESTful API to manage a set of points in a bi-dimensional space.')
    parser.add_argument('--host', default='localhost', help="the host of the api (default='localhost')")
    parser.add_argument('--port', type=int, default=5000, help='the port of the api (default=5000)')
    parser.add_argument('--debug', action='store_true', help='if set, the api runs in debug mode')
    pargs = parser.parse_args()

    app.run(host=pargs.host, port=pargs.port, debug=pargs.debug)
