import types


def api_route(self, *args, **kwargs):
    """Detached method of an Api Flask object to automatically register routes of a class.

    Note:
        This method is attached to an Api Flask object by using `enable_route_decorator`.
        It is then used as a class decorator.
    """
    def wrapper(cls):
        self.add_resource(cls, *args, **kwargs)
        return cls
    return wrapper


def enable_route_decorator(api):
    """Attach the api_route method to an Api Flask object.

    Args:
        api (Api): The Api object.

    Returns:
        (Api): The modified Api object.
    """
    api.route = types.MethodType(api_route, api)
    return api
