from collections import defaultdict
from itertools import chain
from itertools import combinations


def slope(p1, p2):
    """Return the slope of the rect passing through two points.

    Note:
        The slope of a rect perpendicular to the y axis is positive infinity.

    Args:
        p1 (Point): A point.
        p2 (Point): Another point.

    Returns:
        (float): The slope of the rect passing through two points.
    """
    x0, y0 = p1.x, p1.y
    x1, y1 = p2.x, p2.y

    delta_x = (x1 - x0)
    delta_y = (y1 - y0)

    return delta_y / delta_x if delta_x != 0 else float('inf')


class Point:
    """Bi-dimensional point."""
    def __init__(self, x, y):
        """Create a new point.

        Args:
            x (int): The x coordinate.
            y (int): The y coordinate.
        """
        self.x = x
        self.y = y

    def __eq__(self, other):
        """Return `True` if this object equals `other`.

        Args:
            other (object): A object.

        Returns:
             (bool): `True` if `other` is also a `Point` with coordinates equal to this point.
        """
        return isinstance(other, Point) and self.x == other.x and self.y == other.y

    def __hash__(self):
        """Return the hash of this point.

        Returns:
            (int): The hash of this point.
        """
        return hash((self.x, self.y))

    def __str__(self):
        """Return a human-readable string describing this point.

        Returns:
            (string): A human-readable string describing this point.
        """
        return self.__repr__()

    def __repr__(self):
        """Return a human-readable string describing this point.

        Returns:
            (string): A human-readable string describing this point.
        """
        return f'Point(x={self.x}, y={self.y})'


class LineSegment:
    """A set of collinear points.

    Note:
        Two or more points are collinear if they lie on the same rect.
        A LineSegment must contains at least two points.
    """

    def __init__(self, *points):
        """Create a new line segment.

        Note:
            A line segment must contain at least two points.

        Args:
            *points (:list:Point): A list of at least two points.

        Raises:
            AssertionError: If *points contains less than two points.
        """
        assert len(points) > 1
        self.points = set(points)

    def is_collinear_with(self, point):
        """Return `True` if the given point is collinear with this line segment.

        Args:
            point (Point): A point.

        Returns:
            (bool): `True` if the given point is collinear with this line segment.
        """
        iterator = iter(self.points)
        p1 = next(iterator)
        p2 = next(iterator)
        return slope(p1, p2) == slope(p1, point)

    def add_point(self, point):
        """Add a point to this line segment.

        Args:
            point (Point): The point to add.

        Raises:
             ValueError: If the given point is not collinear with this segment.
        """
        if not self.is_collinear_with(point):
            raise ValueError(f'{point} is not collinear with line segment.')

        self.points.add(point)

    def __len__(self):
        """Return the number of points in this line segment.

        Returns:
            (int): The number of points in this line segment.
        """
        return len(self.points)

    def __contains__(self, point):
        """Return `True` if the given point is a member of this line segment.

        Args:
            point (Point): A point.

        Returns:
            (bool): `True` if the given point is a member of this line segment.
        """
        return point in self.points

    def __eq__(self, other):
        """Return `True` if this line segment is equal to the given object.

        Args:
            other (object): The object to compare to this line segment.

        Returns:
            (bool): `True` if `other` is also a LineSegment with the same points of this line line segment.
        """
        return isinstance(other, LineSegment) and self.points == other.points

    def __hash__(self):
        """Return the hash of this line segment.

        Returns:
            (int): The hash of this line segment.
        """
        return hash(frozenset(self.points))

    def __str__(self):
        """Return a human-readable string describing this line segment.

        Returns:
            (string): A human-readable string describing this line segment.
        """
        return self.__repr__()

    def __repr__(self):
        return f'LineSegment(points={self.points})'


class Space:
    """A set of points."""

    def __init__(self, *points):
        """Create a new space with the given points.

        Args:
            *points (:list:Point): A list of points.
        """
        self.points = set(points)

    def clear(self):
        """Remove all points from this space."""
        self.points.clear()

    def __contains__(self, point):
        """Return `True` if the given point is contained in this space.

        Args:
            point (Point): A point.

        Returns:
            (bool): `True` if the given point is contained in this space.
        """
        return point in self.points

    def add_point(self, point):
        """Add a point to this space.

        Args:
            point (Point): A point.
        """
        self.points.add(point)

    def slow_maximal_line_segments(self):
        """Return all maximal line segments that can be build with the points in this space.

        Note:
            This function runs in O(len(`self.points`)^3) time.

        Returns:
            (:set:LineSegment): All maximal line segments that can be build with the points in this space.
        """
        result = set()
        for p1, p2 in combinations(self.points, 2):
            target_slope = slope(p1, p2)
            collinear_points = {p1, p2}.union({p for p in self.points if slope(p1, p) == target_slope})
            result.add(LineSegment(*collinear_points))
        return result

    def maximal_line_segments_collinear_with(self, point):
        """Returns the list of all points collinear with the given point.

        Note:
            This function runs in O(len(`self.points`)) time.

        Args:
            point (Point): A point.

        Returns:
            (:list:Point): The list of all points collinear with the given point.
        """
        slope_dict = defaultdict(list)
        for other_point in self.points:
            if other_point != point:
                slope_dict[slope(point, other_point)].append(other_point)

        line_segments = []
        for points in slope_dict.values():
            points.append(point)
            line_segments.append(LineSegment(*points))
        return line_segments

    def fast_maximal_line_segments(self):
        """Return all maximal line segments that can be build with the points in this space.

        Note:
            This function runs in O(len(`self.points`^2)) time.

        Returns:
            (:set:LineSegment): All maximal line segments that can be build with the points in this space.
        """
        # Create a generator that, for each point, generates the lists of all line segments which are collinear with it.
        # Flatten the generated lists into a single list and then remove duplicated line segments.
        return set(chain.from_iterable(self.maximal_line_segments_collinear_with(point) for point in self.points))

    def line_segments_longer_than(self, n, mode='fast'):
        """Return all maximal line segments in this space longer than `n`.

        Args:
            n (int): The minimum length of the line segments to return.
            mode (str): The string 'fast' or 'slow' referring to the O(`self.points`^2) and O(`self.points`^3)
                implementation, resp.

        Returns:
            (:set:LineSegment): All maximal line segments in this space longer than `n`.
        """
        assert(mode in ('fast', 'slow'))

        if mode == 'fast':
            maximal_line_segments = self.fast_maximal_line_segments
        else:
            maximal_line_segments = self.slow_maximal_line_segments

        return {line_segment for line_segment in maximal_line_segments() if len(line_segment) >= n}
